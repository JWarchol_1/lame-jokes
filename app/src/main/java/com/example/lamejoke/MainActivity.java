package com.example.lamejoke;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {
    private TextView TextField = null;
    private JokesGenerator jokesGenerator = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jokesGenerator = new JokesGenerator(getResources().getConfiguration().locale.getLanguage());
    }

    public void generateLameJoke(View view) {
        TextField = findViewById(R.id.text);

        TextField.setText(jokesGenerator.getJokes());
    }
}
