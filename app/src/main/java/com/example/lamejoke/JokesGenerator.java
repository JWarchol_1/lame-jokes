package com.example.lamejoke;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JokesGenerator {
    private List<String> en;
    private List<String> pl;
    private List<String> de;

    private String lang;
    private Random rand;

    public JokesGenerator(String lang) {
        this.lang = lang;
        en = new ArrayList<>();
        pl = new ArrayList<>();
        de = new ArrayList<>();
        rand = new Random();

        genEnglishJokes();
        genGermanJokes();
        getPolishJokes();
    }

    public String getJokes() {
        if(lang.equals("en"))
            return en.get(rand.nextInt(en.size()));
        else if(lang.equals("pl"))
            return pl.get(rand.nextInt(pl.size()));
        else if(lang.equals("de"))
            return de.get(rand.nextInt(de.size()));
        else
            return en.get(rand.nextInt(en.size()));
    }

    private void genEnglishJokes() {

        en.add("What do you call the security outside of a Samsung Store?\n" +
                "Guardians of the Galaxy.");
        en.add("I have a lot of good jokes about unemployed people...\n" +
                "But none of them work.");
        en.add("What did the guy say to the man who cut off his feet?\n" +
                "Oh no, you've defeeted me!");
        en.add("The man was hit in the head with a can of Sprite.\n" +
                "He's okay, it was a soft drink");
        en.add("What do you call a person without a body and a nose?\n" +
                "Nobody knows!");
        en.add("Why do the French eat snails?\n" +
                "They don't like fast food.");
        en.add("What do you call a bee that was born is the United States?\n" +
                "A USB.");
    }

    private void getPolishJokes() {

        pl.add("Jak się nazywa czerwone działanie?\n" +
                "RedAkcja.");
        pl.add("Na świecie jest 10 radzajów ludzi...\n" +
                "Ci, którzy rozumieją system binarny i ci, którzy go nie rozumieją.");
        pl.add("Ile wynosi całka z mydła? \n" +
                        "Całka z mydła to Żyd.");
        pl.add("Opowiedziałbym żart o Żydzie...\n" +
                "Ale boję się, że go spalę.");
        pl.add("Wchodzi całka oznaczona do pociągu...\n" +
                "A to nie jej przedział.");
        pl.add("Co robi zaatakowany kucharz?\n" +
                "- Wzywa posiłki.");
        pl.add("Co robi murzyn z białą kobietą?\n" +
                "- Wciska jej ciemnotę.");
    }

    private void genGermanJokes() {

        de.add("Jasiu sagt zu einem Papa:\n" +
                "-Dein Vater ist Dumm\n" +
                "Und der Papa auf:\n" +
                "- Ja! Wohl dein!");
        de.add("Blonde kommt in den Laden und sagt:\n" +
                "- Ich wollte, eine Beschwerde einzureichen.\n" +
                "- Ja? Und aus welchem \u200B\u200BGrund?\n" +
                "- Warum wird mein Lays direkt aus dem Ofen ist kalt?");
        de.add("Was sagt ein Hai, nachdem es einen Surfer gefressen hat? - \n" +
                "Nett serviert, so mit Frühstücksbrettchen");
        de.add("Herr Doktor ich komm mir so unglaublich überflüssig vor.\n" +
                "- Dr: Der Nächste bitte!");
        de.add("Geht eine schwangere Frau in eine Bäckerei und sagt: \"Ich krieg ein Brot.\"\n" +
                "- Darauf der Bäcker: Sachen gibt's!");
        de.add("Was haben Frauen und Handgranaten gemeinsam?\n" +
                " - Ziehst du den Ring ab, ist dein Haus weg.");
        de.add("Ein Beamter zum anderen: \"Ich weiß gar nicht, was die Leute gegen uns haben...\n" +
                "Wir tun doch nichts!");
    }
}
