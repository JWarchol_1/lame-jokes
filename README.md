# Lame jokes application

## About application
This application generates 'lame jokes' in English, German and Polish (depending on Your system language - default English). You should only tap on the button ;)

## Author
**Jakub Warchoł** - *school project*

## Licence
This project is licenced under the MIT license - see the [license file](LICENSE.md) for more details.
